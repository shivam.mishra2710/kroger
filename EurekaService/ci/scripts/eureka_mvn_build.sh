#!/bin/bash

set -e

#TODO Script for maven build of eureka

echo "Starting build"

mkdir eureka_jar/target

cd eureka-src

mvn package -Dmaven.test.skip=true

echo "moving jar to output directory"

cp target/*.jar ../eureka_jar/target

echo "Build done"

echo "Moving Dockerfile to output dir"

cp Dockerfile ../eureka_jar
