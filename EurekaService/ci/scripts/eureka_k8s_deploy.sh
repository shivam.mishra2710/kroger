#!/bin/bash

echo "Setting KUBECONFIG"

export  KUBECONFIG=$KUBECONFIG:/home/config

echo "Target cluster"

kubectl cluster-info

cd eureka-src

echo "Deleting deployment if exists"

kubectl delete -f EurekaService.yaml -n dev

sleep 5

echo "deploying"

kubectl create -f EurekaService.yaml -n dev

echo "deployment success!"
