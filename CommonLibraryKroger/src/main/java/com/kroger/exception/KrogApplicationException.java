/*
 * Copyright (C) {year} VMware, LLC
 * https://VMware.com
 * helpnowplus-product-dev@vmware.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.kroger.exception;

/**
 * @author HelpNowPlus-itdev@vmware.com
 *
 */
public class KrogApplicationException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6349619252478845521L;

	/**
	 * 
	 */
	private final String errorMessage;

	/**
	 * 
	 */
	private final String errorCode;

	/**
	 * 
	 */
	private final String serviceCode;

	/**
	 * @param errorMessage
	 * @param errorCode
	 * @param serviceCode
	 */
	public KrogApplicationException(String errorMessage, String errorCode, String serviceCode) {
		super();
		this.errorMessage = errorMessage;
		this.errorCode = errorCode;
		this.serviceCode = serviceCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public String getServiceCode() {
		return serviceCode;
	}

}
