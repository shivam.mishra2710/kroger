/*
 * Copyright (C) {year} VMware, LLC
 * https://VMware.com
 * kumaravinas@vmware.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.kroger.dto;

import java.util.LinkedHashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

/**
 * The Class ApiResponseDto.
 */
@JsonInclude(Include.NON_NULL)
@Data
public class ApiResponseDto {

	/** The data. */
	private Object data;
	
	/** The error. */
	private ErrorDetailDto error;

	/** The data map. */
	@JsonIgnore
	private Map<String, Object> dataMap;

	/**
	 * Instantiates a new api response dto.
	 */
	public ApiResponseDto() {

	}

	/**
	 * Instantiates a new api response dto.
	 *
	 * @param data the data
	 */
	public ApiResponseDto(Object data) {
		super();
		this.data = data;
	}

	/**
	 * Instantiates a new api response dto.
	 *
	 * @param error the error
	 */
	public ApiResponseDto(ErrorDetailDto error) {
		super();
		this.error = error;
	}

	/**
	 * Adds the data.
	 *
	 * @param id the id
	 * @param value the value
	 */
	@JsonIgnore
	public void addData(String id, Object value) {
		if (dataMap == null) {
			dataMap = new LinkedHashMap<>();
			data = dataMap;
		}
		dataMap.put(id, value);
	}

}
