/*
 * Copyright (C) {year} VMware, LLC
 * https://VMware.com
 * helpnowplus-product-dev@vmware.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.kroger.dto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

/**
 * The Class UIErrorPayloadDto.
 *
 * @author HelpNowPlus-itdev@vmware.com
 */
@JsonAutoDetect(fieldVisibility = Visibility.NONE, getterVisibility = Visibility.PUBLIC_ONLY)
@Data
public class UIErrorPayloadDto {

	/** The error message. */
	@JsonProperty("errorMessage")
	private String errorMessage;

	/** The error code. */
	@JsonProperty("errorCode")
	private String errorCode;

	/**
	 * Instantiates a new UI error payload dto.
	 *
	 * @param errorMessage the error message
	 * @param errorCode    the error code
	 */
	@JsonCreator
	public UIErrorPayloadDto(@JsonProperty("errorMessage") String errorMessage,
			@JsonProperty("errorCode") String errorCode) {
		this.errorMessage = errorMessage;
		this.errorCode = errorCode;
	}

	/**
	 * Instantiates a new UI error payload dto.
	 */
	@JsonCreator
	public UIErrorPayloadDto() {

	}

}
