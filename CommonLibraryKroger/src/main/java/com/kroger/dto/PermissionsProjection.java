/*
 * Copyright (C) {year} VMware, LLC
 * https://VMware.com
 * helpnowplus-product-dev@vmware.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kroger.dto;

/**
 * The Interface PermissionsProjection.
 *
 * @author jpannem
 */
public interface PermissionsProjection {

	/**
	 * Gets the roles.
	 *
	 * @return the roles
	 */
	String getRoles();

	/**
	 * Gets the action.
	 *
	 * @return the action
	 */
	String getAction();

}
