/*
 * Copyright (C) {year} VMware, LLC
 * https://VMware.com
 * helpnowplus-product-dev@vmware.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.kroger.util;

import static com.kroger.util.CommonConstants.USERNAME;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component("jwtTokenUtil")
public class JWTTokenUtil {

	/** The Constant log. */
	private static final Logger LOG = LoggerFactory.getLogger(JWTTokenUtil.class);

	@Autowired
	@Qualifier("objectMapper")
	private ObjectMapper mapper;

	/**
	 * @param jwttoken
	 * @param userName
	 * @return
	 */
	public boolean validateUserWithAccessToken(String jwttoken, String userName) {

		Map<String, Object> jwtClaimsMap = getJWTPayloadFromAccessToken(jwttoken);

		if (null != jwtClaimsMap && (jwtClaimsMap.containsKey(USERNAME) && jwtClaimsMap.get(USERNAME) != null)) {

			String userNameFromJWTToken = (String) jwtClaimsMap.get(USERNAME);

			if (userNameFromJWTToken.equalsIgnoreCase(userName)) {
				return true;
			}
		}

		return false;
	}

	/**
	 * @param jwttoken
	 * @param userName
	 * @return
	 */
	public boolean validateUserWithAccessToken(Map<String, Object> jwtClaimsMap, String userName) {

		if (null != jwtClaimsMap && (jwtClaimsMap.containsKey(USERNAME) && jwtClaimsMap.get(USERNAME) != null)) {

			String userNameFromJWTToken = (String) jwtClaimsMap.get(USERNAME);

			if (userNameFromJWTToken.equalsIgnoreCase(userName)) {
				return true;
			}
		}

		return false;
	}

	/**
	 * @param jwtToken
	 * @return
	 */
	public Map<String, Object> getJWTPayloadFromAccessToken(String jwtToken) {
		String accessToken = jwtToken.substring(7);
		String[] accessTokenSegments = accessToken.split("\\.");
		String jwtPayload = null;
		try {

			if (null != accessTokenSegments && accessTokenSegments.length == 3) {
				jwtPayload = accessTokenSegments[1];

				if (null != jwtPayload && !StringUtils.isEmpty(jwtPayload)) {
					byte[] base64decodedBytes = Base64.getDecoder().decode(jwtPayload);

					if (null != base64decodedBytes) {
						return parseJWTPayloadToMap(new String(base64decodedBytes, "utf-8"));
					}
				}
			}

		} catch (UnsupportedEncodingException e) {
			LOG.error("UnsupportedEncodingException in extracting jwtPayload from jwtToken ", e.getMessage(), e);
		}

		return null;
	}

	/**
	 * @param jwtPayload
	 * @return
	 */
	private HashMap<String, Object> parseJWTPayloadToMap(String jwtPayload) {

		HashMap<String, Object> jwtClaimsMap = null;
		try {
			jwtClaimsMap = mapper.readValue(jwtPayload, new TypeReference<Map<String, Object>>() {
			});

		} catch (JsonParseException e) {
			LOG.error("JsonParseException in Parsing jwtToken in JWTTokenUtil", e.getMessage(), e);
		} catch (JsonMappingException e) {
			LOG.error("JsonMappingException in Parsing jwtToken in JWTTokenUtil", e.getMessage(), e);
		} catch (IOException e) {
			LOG.error("IOException in Parsing jwtToken in JWTTokenUtil", e.getMessage(), e);
		}

		return jwtClaimsMap;
	}

}
