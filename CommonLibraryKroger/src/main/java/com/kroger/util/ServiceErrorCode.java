/*
 * Copyright (C) {year} VMware, LLC
 * https://VMware.com
 * kumaravinas@vmware.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.kroger.util;

public enum ServiceErrorCode {

	GENERIC_ERROR("HN10001"),

	USER_NOT_FOUND_ERROR("HN10002"),

	USER_PREF_NOT_FOUND_ERROR("HN10003"),

	TIMEZONE_BAD_REQUEST_ERROR("HN10004"),

	TIMEZONE_EXISTS_ERROR("HN10005"),

	TIMEZONE_ID_NOT_FOUND_ERROR("HN10006"),

	TIMEZONE_NAME_EXISTS_ERROR("HN10007"),

	USER_UNAUTHORIZED_ERROR("HN10008"),

	BAD_DATA_ERROR("HN10009"),

	USER_PREF_EXISTS_ERROR("HN10010"),

	INVALID_USER_PREF_ID_ERROR("HN10011"),

	REQUIRED_FIELD_MISSING_ERROR("HN10012"),

	PASSWORD_NOT_MATCHED_ERROR("HN10013"),

	USER_PERSONALIZED_EXISTS_ERROR("HN10014"),

	INVALID_PERSONALIZEID_EXISTS_ERROR("HN10015"),

	PERSONALIZE_BADDATA_ERROR("HN10016"),

	INVALID_GROUPS_ID_ERROR("HN10017"),

	SAVEDFILTER_ID_NOT_FOUND_ERROR("HN10018"),

	FILTER_NAME_EXISTS_ERROR("HN10019"),

	IMPERSONATE_UNAUTHORIZED_ERROR("HN100020"),

	INVALID_IMPERSONATE_UN_ERROR("HN10021"),

	INVALID_TICKET_ID_ERROR("HN10022"), INVALID_TICKET_ITEM_ID_ERROR("HN10023"), TICKET_FORBIDDEN("HN10024"),
	USER_NOT_ADMIN_ERROR("HN10025"), CONFIG_ERROR("HN10026"), USER_INACTIVE_ERROR("HN10027"),
	INVALID_BOOLEAN_PARAMETER_VALUE_ERROR("HN10028"),

	INVALID_INTEGER_PARAMETER_VALUE_ERROR("HN10029"), INVALID_SORTING_ORDER_ERROR("HN10030"),
	INVALID_APPROVAL_STATE_ERROR("HN10031"), METHOD_NOT_ALLOWED_ERROR("HN10032"), TYPE_MISMATCH_ERROR("HN10033"),
	IMPERSONATION_ERROR("HN10034"), EMPTY_ROLES_ERROR("HN10035"), EMPTY_AUTHORITIES_ERROR("HN10036"),
	ACCESS_TOKEN_INVALID_ERROR("HN10037"), XREMOTEUSER_XIMPERSONATE_HEADERS_SAME_ERROR("HN10038"),
	XREMOTEUSER_EMPTY_ERROR("HN10039"), TICKET_ITEM_FORBIDDEN("HN10040"), USER_HAS_NO_EXPECTED_ROLE("HN10041");

	private String errorCode;

	ServiceErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorCode() {
		return errorCode;
	}
}
