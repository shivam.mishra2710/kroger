/*
 * Copyright (C) {year} VMware, LLC
 * https://VMware.com
 * helpnowplus-product-dev@vmware.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.kroger.util;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import org.apache.commons.lang.StringUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * @author shivammishra2
 *
 */
@Slf4j
public class DateUtil {

	private DateUtil() {
		throw new IllegalStateException("Utility class");
	}

	/**
	 * @param startDate
	 * @return
	 */
	public static Timestamp convertStringToTimestamp(String startDate) {
		try {

			SimpleDateFormat gmtDateFormat = new SimpleDateFormat(CommonConstants.DATE_FORMAT);
			gmtDateFormat.setTimeZone(TimeZone.getTimeZone(CommonConstants.GMT_TIMEZONE));
			Date date = gmtDateFormat.parse(startDate);
			return new Timestamp(date.getTime());
		} catch (ParseException e) {
			log.error("Exception in convertStringToTimestamp:{}", e);
			return null;
		}
	}

	/**
	 * @param date
	 * @return
	 */
	public static Map<String, Object> convertDatetoTimestamp(Date date) {
		Map<String, Object> dateMap = new HashMap<>();
		SimpleDateFormat gmtDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Timestamp timeStampDate = convertStringToTimestamp2(convertDateGMTToStr(date));
		dateMap.put("DTODATE", convertDateGMTToStr(date));
		dateMap.put("TIMESTAMPDATE", timeStampDate);
		return dateMap;
	}

	/**
	 * @param date
	 * @return
	 */
	public static Date convertDatetoGMT(Date date) {
		try {
			SimpleDateFormat gmtDateFormat = new SimpleDateFormat(CommonConstants.DATE_FORMAT);
			gmtDateFormat.setTimeZone(TimeZone.getTimeZone(CommonConstants.GMT_TIMEZONE));
			String dateFrDTO = gmtDateFormat.format(date);
			SimpleDateFormat gmtDateFormat1 = new SimpleDateFormat(CommonConstants.DATE_FORMAT);
			Date date1 = gmtDateFormat1.parse(dateFrDTO);
			date1.toGMTString();
			return new Timestamp(date1.getTime());
		} catch (ParseException e) {
			log.error("Exception in convertDatetoGMT:{}", e);
			return null;
		}
	}

	/**
	 * @param date
	 * @return
	 */
	public static String convertDateGMTToStr(Date date) {
		SimpleDateFormat gmtDateFormat = new SimpleDateFormat(CommonConstants.DATE_FORMAT);
		gmtDateFormat.setTimeZone(TimeZone.getTimeZone(CommonConstants.GMT_TIMEZONE));
		return gmtDateFormat.format(date);

	}

	/**
	 * @param t1
	 * @param t2
	 * @return
	 */
	public static int findDifference(Timestamp t1, Timestamp t2) {

		long milliseconds = t2.getTime() - t1.getTime();
		int seconds = (int) milliseconds / 1000;

		// calculate hours minutes and seconds
		int hours = seconds / 3600;
		seconds = (seconds % 3600) % 60;
		int days = hours / 24;

		return days;

	}

	public static Timestamp convertStringToTimestamp2(String startDate) {
		try {

			SimpleDateFormat gmtDateFormat = new SimpleDateFormat(CommonConstants.DATE_FORMAT);
			Date date = gmtDateFormat.parse(startDate);
			return new Timestamp(date.getTime());
		} catch (ParseException e) {
			return null;
		}

	}

	public static boolean validateJavaDate(String strDate) {
		/* Check if date is 'null' */
		if (StringUtils.isEmpty(strDate)) {
			return true;
		}
		/* Date is not 'null' */
		else {
			/*
			 * Set preferred date format, For example MM-dd-yyyy, MM.dd.yyyy,dd.MM.yyyy etc.
			 */
			SimpleDateFormat sdfrmt = new SimpleDateFormat(CommonConstants.DATE_FORMAT);
			sdfrmt.setLenient(false);
			/*
			 * Create Date object parse the string into date
			 */
			try {
				Date javaDate = sdfrmt.parse(strDate);
				log.info(strDate + " is valid date format");
				if (new Date().before(javaDate)) {
					return false;
				}
			}
			/* Date format is invalid */
			catch (ParseException e) {
				log.info(strDate + " is Invalid Date format");
				return false;
			}
			/* Return true if date format is valid */
			return true;
		}
	}

}
