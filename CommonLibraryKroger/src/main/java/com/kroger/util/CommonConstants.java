/*
 * Copyright (C) {year} VMware, LLC
 * https://VMware.com
 * helpnowplus-product-dev@vmware.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.kroger.util;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.springframework.http.HttpHeaders;

public class CommonConstants {

	/** The Constant TECHNICAL. */
	public static final String TECHNICAL = "TECHNICAL";

	/** The Constant FAILURE. */
	public static final String FAILURE = "Failure";

	/** The Constant SUCCESS. */
	public static final String SUCCESS = "Success";

	/** The Constant UNAUTHORIZED_ACCESS. */
	public static final String UNAUTHORIZED_ACCESS = "UNAUTHORIZED ACCESS";

	/** The Constant INVALID_TOKEN. */
	public static final String INVALID_TOKEN = "Invalid Token";

	/** The Constant INVALID_TOKEN. */
	public static final Integer UNAUTHORIZED_ERRCODE = 401;

	public static final String PAGE_404 = "/404.html";

	public static final String AUTHERIZATION_STR = "Authorization";

	public static final String CLOSED_STATUS = "Closed";

	public static final String CANCELLED_STATUS = "Cancelled";

	public static final String ERROR_EMAIL = "ERROR_EMAIL";

	/**
	 * Instantiates a new common constants.
	 */
	private CommonConstants() {

	}

	public static final String SOURCE = "broker";

	public static final String MODIFIED_BY = "portal";

	public static final String PERMISSION_ACTION = "R";

	public static final String VALID_USER_KEY = "validUser";

	public static final String REPSONSE_KEY = "response";

	public static final String XREMOTE_USER_HEADER = "X-Remote-User";

	public static final String XIMPERSONATE_USER_HEADER = "X-Impersonate-User";

	public static final String AUTHORIZATION_HEADER = "Authorization";

	public static final String CONNECTION_TIME_OUT = "Connection timed out";

	public static final String CONNECTION_TIME_OUT_HTTP_CODE = "599";

	public static final String UNAUTHORIZED_ACCESS_HTTP_CODE = "401";

	public static final String NOT_FOUND_HTTP_CODE = "404";

	public static final String ERROR = "_Error";

	public static final String SAP_ERROR = "SAP_ERROR";

	public static final String NO_DATA_IN_SAP = "NO_DATA_IN_SAP";

	public static final String SAP_URL = "SAP_Url";

	public static final String USER_IMPERSONATION_ERROR_MSG = "User does not have Impersonation right";

	public static final String ERROR_KEY = "error";

	public static final String ERROR_MESSAGE_KEY = "errorMessage";

	public static final String USERNAME_INVLALID_MSG = "Username is invalid";

	public static final String ACCESS_TOKEN_INVALID_MSG = "'s Access token is invalid";

	public static final String BS_JSON_PARSING_ERROR = "BS_JSON_PARSING_ERROR";

	public static final String DATABASE_ERROR = "DATABASE_ERROR";

	public static final String COOKIE_HEADER_FROM_BS = "Cookie";

	public static final String BS_RESPONSE_KEY = "BS_Response";

	public static final String CSRF_TOKEN_VALIDATION_FAILED_MSG = "CSRF token validation failed";

	public static final String X_REMOTE_USER_MANDATORY_ERROR_MSG = "X-Remote-User is a mandatory field for header";

	public static final String X_REMOTE_USER_INVALID_ERROR_MSG = "X-Remote-User header value is invalid";

	public static final String X_IMPERSONATE_USER_INVALID_ERROR_MSG = "X-Impersonate-User header value is invalid";

	public static final String REQUEST_FROM_UI = "Request_From_UI";

	public static final String REQUEST_TO_SAP = "Request_To_SAP";

	public static final String RESPONSE_TO_UI = "Response_To_UI";

	public static final String RESPONSE_FROM_SAP = "Response_From_SAP";

	public static final String UTF_8 = "UTF-8";

	public static final String SAP_MESSAGE = "sap-message";

	public static final String MESSAGE = "message";

	public static final String UI_PAYLOAD = "UI_Payload";

	public static final String INTERNAL_SERVER_ERROR = "500";

	public static final String BAD_REQUEST_ERROR_CODE = "400";

	public static final String SPRING_PROFILE_DEVELOPMENT = "DEV";

	public static final String SPRING_PROFILE_UAT = "UAT";

	public static final String SPRING_PROFILE_STAGE = "STAGE";

	public static final String USERNAME = "userName";

	public static final String FIRST_NAME = "firstName";

	public static final String LAST_NAME = "lastName";

	public static final String EMAIL = "email";

	/** The Constant CLIENT_ID. */
	public static final String CLIENT_ID = "client_id";

	public static final int DEFAULT_PAGE_SIZE = 20;

	public static final String DEFAULT_ORDER_BY_FIELD = "firstName";

	public static final String ORDER_BY = "orderBy";

	public static final String ORDER = "order";

	public static final String ORDER_ASC = "asc";

	public static final String ORDER_DESC = "desc";

	public static final String START_AT = "startAt";
	
	public static final String FROM = "from";

	public static final String MAX_RESULTS = "maxResults";

	public static final String TOWER_ID = "towerId";

	public static final String ACTIVE = "active";

	public static final Pattern USERNAME_PATTERN = Pattern.compile("(?i)[A-Z0-9_.-]*");

	public static final String AUTHORITIES = "authorities";

	public static final String BASE_PACKAGE = "com.kroger";
	public static final String DATASOURCE = "helpNowDataSource";
	public static final String TRANSACTION_MANAGER = "helpNowTransactionManager";
	public static final String ENTITYMANAGER_FACTORY = "helpNowEntityManagerFactory";
	public static final String BASE_REPOSITORIES_PACKAGE = "com.kroger.repository";

	public static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
	public static final String GMT_TIMEZONE = "GMT";
	public static final String DATEFORDTO = "DTODATE";
	public static final String TIMESTAMPDATE = "TIMESTAMPDATE";

	public static final Long TICKET_OPEN_ID = 1L;
	public static final Long TICKET_CLOSED_ID = 4L;
	public static final Long TICKET_CANCELLED_ID = 5L;

	public static final Long MANAGER_CONSTANT_TRUE = 1L;
	public static final Long MANAGER_CONSTANT_FALSE = 0L;

	/**
	 * Notification Types
	 */
	public static final String NEW_TICKET_NOTIFICATION = "NewTicket";
	public static final String UPDATE_TICKET_NOTIFICATION = "UpdateTicket";
	public static final String DELETE_TICKET_NOTIFICATION = "DeleteTicket";
	public static final String NEW_TICKET_ITEM_NOTIFICATION = "TicketItemCreated";
	public static final String UPDATE_TICKET_ITEM_NOTIFICATION = "TicketItemUpdated";
	public static final String NEW_TASK_NOTIFICATION = "TaskCreated";
	public static final String UPDATE_TASK_NOTIFICATION = "TaskUpdated";
	public static final String NEW_ATTACHMENT_NOTIFICATION = "NewAttachment";
	public static final String SEARCH_NOTIFICATION = "SEARCH";

	/**
	 * ActivityLog Event Types
	 */
	public static final String ACTIVITY_TYPE_CREATED = "CREATED";
	public static final String ACTIVITY_TYPE_DELETED = "DELETED";
	public static final String ACTIVITY_TYPE_UPDATED = "UPDATED";

	/**
	 * ActivityLog Tracking Tables
	 */
	public static final String TICKET_TABLE = "tickets";
	public static final String TICKET_ITEM_TABLE = "ticket_items";
	public static final String TICKET_ITEM_TASK_TABLE = "ticket_item_tasks";
	public static final String WATCHLIST_TABLE = "watchlists";
	public static final String COMMENTS_TABLE = "comments";
	public static final String ATTACHMENTS_TABLE = "attachments";
	public static final String NOTIFICATION_TABLE = "notifications";
	public static final String APPROVAL_TABLE = "approvals";
	
	public static final Map<String, String> ACTIVITY_LOG_FIELD_NAMES_MAP;

	static {
		final Map<String, String> fieldNamesMap = new HashMap<>();
		fieldNamesMap.put("isPrivate", "private");
		fieldNamesMap.put("ticketStages", "ticketStage");
		fieldNamesMap.put("ticketStageId", "ticketStage");
		fieldNamesMap.put("assignToUser", "assignedTo");
		fieldNamesMap.put("ticketType", "ticketType");
		fieldNamesMap.put("source", "contactType");
		ACTIVITY_LOG_FIELD_NAMES_MAP = Collections.unmodifiableMap(fieldNamesMap);
	}

	public static final String DATA_VALIDATION_FAILED = "Data validation failed";
	public static final String USER_ACCESS_AND_ROLE_VALIDATION_FAILED = "User access and role validation failed";

	public static final String NOT_A_VALID_TICKET_ID = "%s is not a valid ticket id";
	public static final String USER_ID_DOES_NOT_EXISTS = "%s userId doesn't exist in system";
	public static final String ROLE_NAME_DOES_NOT_EXISTS = "%s role name doesn't exist in system";

	public static final String ALL = "all";
	public static final String INVALID_BOOLEAN_PARAMETER_VALUE_MESSAGE = "Please enter valid value for %s parameter";
	public static final String INVALID_INTEGER_PARAMETER_VALUE_MESSAGE = "%s must be non-negative integer, please enter valid value";
	public static final String INVALID_SORTING_ORDER_MESSAGE = "Please enter valid sorting order(ASC, DESC).";

	public static final String INVALID_GROUP_BLANK_MESSAGE = "Group cannot be blank";
	public static final String INVALID_TASK_NAME_BLANK_MESSAGE = "Task name cannot be blank";

	public static final String APPROVAL_REQUEST = "Requested";
	public static final String APPROVAL_APPROVED = "Approved";
	public static final String APPROVAL_REJECTED = "Rejected";

	public static final String EMAIL_TYPE_ERROR = "Error";

	public static final String CONFIGURATION_MISSING_MSG = "Configuration missing for %s type";
	public static final String EXCEPTION_TEMPLATE_CONFIG_MSG = "Exception Template is either missing or its content is empty";
	public static final String EXCEPTION_EMAIL_MISSING_CONFIG_MSG = "Exception email config is missing";

	public static final String EMAIL_TEMPLATE_REQUEST_HEADERS = "headers";
	public static final String EMAIL_TEMPLATE_REQUEST_URL = "requestUrl";
	public static final String EMAIL_TEMPLATE_IS_REQUEST_BODY = "isRequestBody";
	public static final String EMAIL_TEMPLATE_REQUEST_BODY = "requestBody";
	public static final String EMAIL_TEMPLATE_EXCEPTION_MESSAGE = "exceptionMessage";
	public static final String EMAIL_TEMPLATE_EXCEPTION_TRACE = "exceptionTrace";
	public static final String EMAIL_TEMPLATE_RESPONSE_BODY = "responseBody";

	public static final List<String> DISPLAY_REQUEST_HEADERS = Collections.unmodifiableList(Arrays.asList(
			HttpHeaders.AUTHORIZATION, CommonConstants.XREMOTE_USER_HEADER, CommonConstants.XIMPERSONATE_USER_HEADER,
			HttpHeaders.ACCEPT, HttpHeaders.CONTENT_TYPE, HttpHeaders.HOST, HttpHeaders.USER_AGENT));

	public static final String API_URL = "URL";

	public static final String USER_HAS_NO_EXPECTED_ROLE = "%s user has no an %s role";
	public static final String XREMOTE_USER_NOT_ADMIN = "XRemoteUser is not an admin user";
	public static final String XREMOTE_USER_IS_NOT_OF_ROLE_USER = "XRemoteUser is not %s user";
	public static final String USER_IS_NOT_OF_ROLE_USER = "User is not %s role user";
	public static final String SUPER_ADMIN_ROLE = "Super_Admin";
	public static final String FULFILLER_ROLE = "Fulfiller";
	public static final String IMPERSONATE_ROLE = "Impersonator";

	public static final String SIZE = "size";

	public static final String NOT_A_VALID_USER_ID = "Not a valid userId";
}
