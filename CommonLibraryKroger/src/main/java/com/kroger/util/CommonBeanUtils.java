/*
 * Copyright (C) {year} VMware, LLC
 * https://VMware.com
 * helpnowplus-product-dev@vmware.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.kroger.util;

import java.beans.PropertyDescriptor;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.stereotype.Component;

@Component("beanUtils")
public class CommonBeanUtils {

	public void copy(Object source, Object target) {
		String[] nullProperties = getNullPropertyNames(source);
		BeanUtils.copyProperties(source, target, nullProperties);
	}

	private String[] getNullPropertyNames(Object source, String... extraFields) {

		final BeanWrapper beanWrapper = new BeanWrapperImpl(source);
		PropertyDescriptor[] propertyDescriptors = beanWrapper.getPropertyDescriptors();
		Set<String> propertyNames = new HashSet<>();

		for (java.beans.PropertyDescriptor propertyDescriptor : propertyDescriptors) {
			Object srcValue = beanWrapper.getPropertyValue(propertyDescriptor.getName());
			if (srcValue == null) {
				propertyNames.add(propertyDescriptor.getName());
			}
		}

		for (String field : extraFields) {
			propertyNames.add(field);
		}
		return propertyNames.toArray(new String[propertyNames.size()]);
	}

	/**
	 * @param text
	 * @return
	 */
	public boolean isNumeric(String text) {
		if (StringUtils.isNotBlank(text)) {
			return text.chars().allMatch(Character::isDigit);
		} else {
			return false;
		}
	}
}
