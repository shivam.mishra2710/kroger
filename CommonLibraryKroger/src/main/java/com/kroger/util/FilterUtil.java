/*
 * Copyright (C) {year} VMware, LLC
 * https://VMware.com
 * helpnowplus-product-dev@vmware.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.kroger.util;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

/**
 * @author shivammishra2
 *
 */
@Slf4j
public class FilterUtil {

	private FilterUtil() {
		throw new IllegalStateException("Utility class");
	}

	/**
	 * @param data
	 * @param filter
	 * @return
	 * @throws IOException
	 */
	public static Object applyFilters(Object data, String filter) throws IOException {
		List<String> filterist = Arrays.asList(filter.split(","));
		List<String> restList = new ArrayList<>();
		Field[] restFields = data.getClass().getDeclaredFields();
		for (Field f : restFields) {
			restList.add(f.getName());
		}
		restList.removeAll(filterist);
		for (String f : restList) {
			invokeSetter(data, f, null);
		}
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.setSerializationInclusion(Include.NON_NULL);
		return objectMapper.readValue(objectMapper.writeValueAsString(data), Object.class);
	}

	/**
	 * @param obj
	 * @param variableName
	 * @param variableValue
	 */
	private static void invokeSetter(Object obj, String variableName, Object variableValue) {
		/*
		 * variableValue is Object because value can be an Object, Integer, String,
		 * etc...
		 */
		try {
			/**
			 * Get object of PropertyDescriptor using variable name and class Note: To use
			 * PropertyDescriptor on any field/variable, the field must have both `Setter`
			 * and `Getter` method.
			 */
			PropertyDescriptor objPropertyDescriptor = new PropertyDescriptor(variableName, obj.getClass());
			/* Set field/variable value using getWriteMethod() */
			objPropertyDescriptor.getWriteMethod().invoke(obj, variableValue);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| IntrospectionException e) {
			/*
			 * Java 8: Multiple exception in one catch. Use Different catch block for lower
			 * version.
			 */
			log.error("Exception in invokeSetter:{}", e);
		}
	}
}
