/*
 * Copyright (C) {year} VMware, LLC
 * https://VMware.com
 * helpnowplus-product-dev@vmware.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.kroger.util;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.junit4.SpringRunner;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kroger.exception.KrogApplicationException;
import com.kroger.util.CommonConstants;
import com.kroger.util.JWTTokenUtil;



@RunWith(SpringRunner.class)
public class JWTTokenUtilTest {

	public static Logger logger = LoggerFactory.getLogger(JWTTokenUtilTest.class);
	
	@Mock
	private ObjectMapper mapper;
	
	@InjectMocks
	JWTTokenUtil jwtTokenUtil;
	
	public static final String ACCESS_TOKEN = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJmaXJzdE5hbWUiOiJTcmluaXZhcyIsImxhc3ROYW1lIjoiU3Vua2EiLCJ1c2VyX25hbWUiOiJzc3Vua2EiLCJzY29wZSI6WyJyZWFkIl0sImlkIjoxLCJ1c2VyTmFtZSI6InNzdW5rYSIsImV4cCI6MTU0OTM1MTk4NSwiYXV0aG9yaXRpZXMiOlsiU3VwZXJfQWRtaW4iLCJFbmRfVXNlciIsIkZ1bGZpbGxlciIsIk1hbmFnZV9DYXRhbG9nIiwiSW1wZXJzb25hdG9yIiwiTWFuYWdlX0dyb3VwIl0sImVtYWlsIjoic3N1bmthQHZtd2FyZS5jb20iLCJqdGkiOiIxNjJmNzM0Yy0zMjRhLTRjYTEtOTZkZS1iNzdkYzk2NGUzMDEiLCJjbGllbnRfaWQiOiJobm9wZW5zb3VyY2UifQ.JqrnUNUhOfwgb3uyRBb8PY7cVJLOZhNvD9U4XhuMgno";
	public static final String USERNAME = "mockUser";
	
	@Before
	public void setUp(){
	}
	
	@Test
	public void testValidateUserWithAccessToken() throws KrogApplicationException, IOException {
		HashMap<String, Object> tokenMap = new HashMap<>();
		tokenMap.put(CommonConstants.USERNAME, USERNAME);
		when(mapper.readValue(isA(String.class), isA(TypeReference.class))).thenReturn(tokenMap);
		assertTrue(jwtTokenUtil.validateUserWithAccessToken(ACCESS_TOKEN,USERNAME));
	}
	
	@Test
	public void testInvalidJWTPayloadValidateUserWithAccessToken() throws KrogApplicationException, IOException {
		assertFalse(jwtTokenUtil.validateUserWithAccessToken(ACCESS_TOKEN,USERNAME));
	}
	
	@Test
	public void testValidateUserWithAccessTokenForMap() throws KrogApplicationException, IOException {
		Map<String, Object> jwtClaimsMap = new HashMap<>();
		jwtClaimsMap.put(CommonConstants.USERNAME, USERNAME);
		assertTrue(jwtTokenUtil.validateUserWithAccessToken(jwtClaimsMap,USERNAME));
	}
	
	/*@Test
	public void testBuildSuccessJson() throws HNApplicationException, JsonProcessingException {
		when(mapper.writeValueAsString(isA(Map.class))).thenReturn(StringUtils.EMPTY);
		assertNotNull(jwtTokenUtil.buildSuccessJson(StringUtils.EMPTY));
	}
	
	@Test
	public void testBuildErrorResponseMap() throws HNApplicationException, JsonProcessingException {
		assertNotNull(jwtTokenUtil.buildErrorResponseMap(StringUtils.EMPTY,ServiceErrorCode.GENERIC_ERROR.getErrorCode()));
	}*/
}
