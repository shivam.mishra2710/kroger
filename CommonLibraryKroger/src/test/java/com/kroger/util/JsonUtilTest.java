/*
 * Copyright (C) {year} VMware, LLC
 * https://VMware.com
 * helpnowplus-product-dev@vmware.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.kroger.util;

import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.when;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.junit4.SpringRunner;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kroger.dto.UIErrorJSONDto;
import com.kroger.exception.KrogApplicationException;
import com.kroger.util.JsonUtil;
import com.kroger.util.ServiceErrorCode;


@RunWith(SpringRunner.class)
public class JsonUtilTest {

	public static Logger logger = LoggerFactory.getLogger(JsonUtilTest.class);
	
	@Mock
	private ObjectMapper mapper;
	
	@InjectMocks
	JsonUtil jsonUtil;
	
	@Before
	public void setUp(){
	}
	
	@Test
	public void testBuildErrorJson() throws KrogApplicationException, JsonProcessingException {
		when(mapper.writeValueAsString(isA(UIErrorJSONDto.class))).thenReturn(StringUtils.EMPTY);
		assertNotNull(jsonUtil.buildErrorJson(StringUtils.EMPTY,ServiceErrorCode.GENERIC_ERROR.getErrorCode()));
	}
	
	@Test
	public void testBuildSuccessJson() throws KrogApplicationException, JsonProcessingException {
		when(mapper.writeValueAsString(isA(Map.class))).thenReturn(StringUtils.EMPTY);
		assertNotNull(jsonUtil.buildSuccessJson(StringUtils.EMPTY));
	}
	
	@Test
	public void testBuildErrorResponseMap() throws KrogApplicationException, JsonProcessingException {
		assertNotNull(jsonUtil.buildErrorResponseMap(StringUtils.EMPTY,ServiceErrorCode.GENERIC_ERROR.getErrorCode()));
	}
}
