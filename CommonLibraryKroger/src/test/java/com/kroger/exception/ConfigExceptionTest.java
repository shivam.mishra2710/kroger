/*
 * Copyright (C) {year} VMware, LLC
 * https://VMware.com
 * helpnowplus-product-dev@vmware.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.kroger.exception;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.kroger.exception.ConfigException;
import com.kroger.util.ServiceErrorCode;

public class ConfigExceptionTest {

	private ConfigException configException;

	private final String CONFIG_BAD_REQUEST_ERROR = "Config Bad Request Exception";

	/**
	 * 
	 */
	@Test
	public void testGetCode() {
		configException = new ConfigException(CONFIG_BAD_REQUEST_ERROR, "Config Bad Request Exception",
				ServiceErrorCode.CONFIG_ERROR);
		assertEquals(ServiceErrorCode.CONFIG_ERROR, configException.getCode());
	}

	/**
	 * 
	 */
	@Test
	public void testGetUserMessage() {
		configException = new ConfigException(CONFIG_BAD_REQUEST_ERROR, "Config Bad Request Exception",
				ServiceErrorCode.CONFIG_ERROR);
		assertEquals("Config Bad Request Exception", configException.getUserMessage());
	}
}
