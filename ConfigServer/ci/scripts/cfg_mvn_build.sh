#!/bin/bash

set -e

echo "Starting build"

mkdir cfgsvr_jar/target

cd cfgsvr-src

#skipping tests for time being

mvn package -Dmaven.test.skip=true

echo "moving jar to output directory"

cp target/ConfigServer-0.0.1.jar ../cfgsvr_jar/target

echo "Build done"

echo "Moving Dockerfile to output dir"

cp Dockerfile ../cfgsvr_jar
