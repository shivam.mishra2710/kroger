package com.kroger.service;

import java.util.List;
import org.apache.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kroger.exception.ProductNotFoundException;
import com.kroger.model.Product;
import com.kroger.repository.KrogerProdRepo;
import com.kroger.response.ProductResponse;
import com.kroger.util.CommonConstants;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class KrogerService {

	@Autowired
	KrogerProdRepo krogerProdRepo;

	@Autowired
	ProductResponse productResponse;

	public List<Product> findAllProducts() {
		
		log.info("Inside find all service");
		
		return (List<Product>) krogerProdRepo.findAll();

	}

	public Product findByProductId(String prodId) {
		
		return krogerProdRepo.findById(prodId)
				.orElseThrow(() -> new ProductNotFoundException("Product not found for an id : "+prodId));
	}

	public ProductResponse saveProduct(Product product) {
		
		log.info("Inside save products service");
		
		krogerProdRepo.save(product);
		productResponse.setStatusCode(HttpStatus.SC_ACCEPTED);
		productResponse.setStatusMsg(CommonConstants.SUCCESS);
		productResponse.setResponse("Product id : " + product.getProdId() + " inserted successfully");
		
		return productResponse;

	}

	public ProductResponse deleteProduct(String productId) {
		
		log.info("Inside delete by id service");

		if (krogerProdRepo.existsById(productId)) {
			krogerProdRepo.deleteById(productId);
				productResponse.setStatusCode(HttpStatus.SC_ACCEPTED);
				productResponse.setStatusMsg(CommonConstants.SUCCESS);
				productResponse.setResponse("Product id : " + productId + " deleted successfully");
		 } else {
			 throw new ProductNotFoundException("User not found for an id : "+productId);
			}
		return productResponse;
	}

}
