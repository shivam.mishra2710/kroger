package com.kroger.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.kroger.model.Product;
import com.kroger.response.ProductResponse;
import com.kroger.service.KrogerService;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class KrogerController extends KrogerBaseController{
	 
	@Autowired
	KrogerService krogerService;
	
	@GetMapping("/findallproducts")
	public List<Product> findAllProducts() {
		
		log.info("Inside of find all service");
		return krogerService.findAllProducts();
	}
	
	@GetMapping("/Hello")
	public String  Hello() {
		
		
		return "Hello";
	}
	
	@GetMapping("/findprodbyid/{prodId}")
	public Product findByUserId(@PathVariable("prodId") String prodId)  {
			
			return krogerService.findByProductId(prodId);
	}
	
	 @PostMapping("/saveprod") 
	 public ProductResponse saveProduct(@RequestBody Product product) {
		
		 log.info("Inside save product service"); 
		 return krogerService.saveProduct(product); 
	}
	
	 @DeleteMapping("/deleteproduct/{id}") 
	 public ProductResponse deleteUser(@PathVariable("id") String prodId) { 
		
		 log.info("Inside delete by id service");
		 return  krogerService.deleteProduct(prodId); 
	}
	
}
