package com.kroger.repository;

import org.springframework.data.cassandra.repository.config.EnableCassandraRepositories;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.kroger.model.Product;

@RepositoryRestResource
@EnableCassandraRepositories
public interface KrogerProdRepo extends CrudRepository<Product, String> {
	
	
}
