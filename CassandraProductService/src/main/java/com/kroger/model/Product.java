package com.kroger.model;

import java.io.Serializable;

import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;
import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
@Table
public class Product implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@PrimaryKey 
	@Column("prodid")
	private String prodId;

	@Column("prodname")
	private String prodName;
	
	@Column("prodprice")
	private int prodPrice;
}
