package com.kroger.repository;

import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.stereotype.Repository;

import com.kroger.model.User;

@Repository
public interface UserRepository extends Neo4jRepository<User, String> {
	
	User findByFirstName(String firstName);
	
	User findByLastName(String lastName);
	
	User findByUserId(String userId);
}
