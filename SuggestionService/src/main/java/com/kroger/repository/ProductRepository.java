package com.kroger.repository;

import java.util.Collection;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.kroger.model.Product;

@Repository
public interface ProductRepository extends Neo4jRepository<Product, String>{

	Product findByProdName(@Param("prodName") String prodName);
	
	Product findByProdId(@Param("prodId") String prodId);

    Collection<Product> findByProdNameLike(@Param("prodName") String prodName);

    @Query("MATCH (m:Product)<-[r:LIKES]-(a:User) RETURN m,r,a LIMIT {limit}")
    Collection<Product> graph(@Param("limit") int limit);
    
    @Query("MATCH (prod:Product),(me:User)\r\n" + 
    		"WHERE prod.prodId = {prodId}\r\n" + 
    		"AND me.userId = {userId}\r\n" + 
    		"MERGE (me)-[l:LIKES]->(prod)\r\n" + 
    		"ON CREATE SET l.score = {AddScore}\r\n" + 
    		"ON MATCH SET l.score = l.score + {AddScore}\r\n" + 
    		"RETURN 0")
    void createRelationship(@Param("userId")String userId,@Param("prodId")String prodId,@Param("AddScore")int score );
}
