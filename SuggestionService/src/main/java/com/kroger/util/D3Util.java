package com.kroger.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.kroger.model.Activity;
import com.kroger.model.Product;

public class D3Util {
	
	public static Map<String, Object> toD3Format(Collection<Product> productList) {
		List<Map<String, Object>> nodes = new ArrayList<>();
		List<Map<String, Object>> rels = new ArrayList<>();
		int i = 0;
		Iterator<Product> result = productList.iterator();
		while (result.hasNext()) {
			Product product = result.next();
			nodes.add(map("title", product.getProdName(), "label", "product"));
			int target = i;
			i++;
			for (Activity role : product.getActivity()) {
				Map<String, Object> user = map("prodName", role.getUser().getFirstName(), "label", "user");
				int source = nodes.indexOf(user);
				if (source == -1) {
					nodes.add(user);
					source = i++;
				}
				rels.add(map("source", source, "target", target));
			}
		}
		return map("nodes", nodes, "links", rels);
	}
	
	private static Map<String, Object> map(String key1, Object value1, String key2, Object value2) {
		Map<String,Object> result =  new HashMap<String, Object>(2);
		result.put(key1, value1);
		result.put(key2, value2);
		return result;
	}

}
