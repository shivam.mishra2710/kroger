package com.kroger.model;

import java.util.List;

import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@NodeEntity
@Data
public class Product {

	private String prodName;
	
	@Id
	private String prodId;
	
	private String prodDesc;
	
	private int prodPrice;
	
	@JsonIgnoreProperties("product")
	@Relationship(type = "LIKES", direction = Relationship.INCOMING)
	private List<Activity> activity;
}
