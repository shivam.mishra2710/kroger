package com.kroger.model;

import java.util.ArrayList;
import java.util.List;

import org.neo4j.ogm.annotation.EndNode;
import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.RelationshipEntity;
import org.neo4j.ogm.annotation.StartNode;

import lombok.Data;

@RelationshipEntity(type = "LIKES")
@Data
public class Activity {

	@Id
	@GeneratedValue 
	private Long id;
	
	private int score;

	@StartNode
	private User user;

	@EndNode
	private Product product;
}
