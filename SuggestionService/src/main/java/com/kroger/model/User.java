package com.kroger.model;

import java.util.ArrayList;
import java.util.List;

import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;
import org.springframework.stereotype.Component;

import lombok.Data;

@NodeEntity
@Data
public class User {

	@Id
	private String userId;
	
	private String firstName;
	
	private String lastName;
	
	private String type;
	
	@Relationship(type = "LIKES")
	private List<Product> products = new ArrayList<>();
}
