package com.kroger.service;

import org.apache.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kroger.dto.GenericResponse;
import com.kroger.model.User;
import com.kroger.repository.UserRepository;
import com.kroger.util.CommonConstants;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class UserService {
	
	
	UserRepository userRepository;
	
	GenericResponse response;
	
	@Autowired
	public UserService(UserRepository userRepository, GenericResponse response) {
		super();
		this.userRepository = userRepository;
		this.response = response;
	}
	
	public User findByUserId(String userId) {
		return userRepository.findByUserId(userId);
	}

	public GenericResponse saveUser(User user) {
		
		log.info("Inside save user service");
		try {
			userRepository.save(user);
		} catch (Exception e) {
			e.printStackTrace();
		}
		response.setStatusCode(HttpStatus.SC_ACCEPTED);
		response.setStatusMsg(CommonConstants.SUCCESS);
		response.setResponse("User id : " + user.getUserId() + " Node created successfully");
		
		return response;

	}
}
