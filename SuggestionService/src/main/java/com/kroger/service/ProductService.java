package com.kroger.service;

import java.util.Collection;
import java.util.Map;

import org.apache.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.kroger.dto.GenericResponse;
import com.kroger.model.Product;
import com.kroger.model.User;
import com.kroger.repository.ProductRepository;
import com.kroger.util.CommonConstants;
import com.kroger.util.D3Util;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ProductService {
	
	@Autowired
	ProductRepository productRepository;
	
	@Autowired
	GenericResponse response;
	
	public GenericResponse saveProduct(Product product) {
		
		log.info("Inside save product service");
		
		productRepository.save(product);
		response.setStatusCode(HttpStatus.SC_ACCEPTED);
		response.setStatusMsg(CommonConstants.SUCCESS);
		response.setResponse("Product id : " + product.getProdId() + " Node created successfully");
		
		return response;

	}
	
	@Transactional(readOnly = true)
    public Product findByProdName(String prodName) {
        return productRepository.findByProdName(prodName);
    }

    @Transactional(readOnly = true)
    public Collection<Product> findByProdNameLike(String prodName) {
        return productRepository.findByProdNameLike(prodName);
    }
    
    @Transactional(readOnly = true)
    public Product findByProdId(String prodId) {
        return productRepository.findByProdId(prodId);
    }

    @Transactional(readOnly = true)
    public Map<String, Object>  graph(int limit) {
        Collection<Product> result = productRepository.graph(limit);
        return D3Util.toD3Format(result);
    }
    
    public void createRelationship(String userId,String prodId,int score) {
    	productRepository.createRelationship(userId, prodId,score);
    }

}
