package com.kroger.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kroger.dto.GenericResponse;
import com.kroger.model.Activity;
import com.kroger.model.Product;
import com.kroger.model.User;
import com.kroger.service.ProductService;
import com.kroger.service.UserService;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class SuggestionController {
	
	@Autowired
	ProductService productService;
	
	@Autowired
	UserService userService;
	
	@GetMapping("/graph")
	public Map<String, Object> graph(@RequestParam(value = "limit",required = false) Integer limit) {
		return productService.graph(limit == null ? 100 : limit);
	}
	
	@PostMapping("/createUser") 
	public GenericResponse createUser(@RequestBody User user) {

		log.info("Inside save user service");
		return userService.saveUser(user); 
	}
	
	@PostMapping("/createProduct") 
	public GenericResponse createProduct(@RequestBody Product product) {

		log.info("Inside create product service"); 
		return productService.saveProduct(product); 
	}
	
	@GetMapping("/createUserActivity/{userId}/{prodId}/{score}")
	public GenericResponse createUserActivity(@PathVariable("userId") String userId,@PathVariable("prodId") String prodId
			,@PathVariable("score") int score) {
		log.info("Inside create user activity service");
		productService.createRelationship(userId, prodId,score);
		return null;
	}
}
